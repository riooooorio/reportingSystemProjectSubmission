package com.antra.evaluation.reporting_system.pojo.api;

public class ExcelResponse {
    private String fileId;
    private String fileName;
	private String fileSize;
	private String generatedTime;
	
	
	public ExcelResponse() {}
	public ExcelResponse(String fileId, String fileName, String fileSize, String generatedTime) {
		super();
		this.fileId = fileId;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.generatedTime = generatedTime;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getGeneratedTime() {
		return generatedTime;
	}
	public void setGeneratedTime(String generatedTime) {
		this.generatedTime = generatedTime;
	}

    
}
