package com.antra.evaluation.reporting_system.pojo.report;

public class ExcelFile {
	 private String fileId;
	    private String fileName;
		private String fileSize;
		private String generatedTime;
		private String filePath;
		
		public String getFileId() {
			return fileId;
		}
		public void setFileId(String fileId) {
			this.fileId = fileId;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getFileSize() {
			return fileSize;
		}
		public void setFileSize(String fileSize) {
			this.fileSize = fileSize;
		}
		public String getGeneratedTime() {
			return generatedTime;
		}
		public void setGeneratedTime(String generatedTime) {
			this.generatedTime = generatedTime;
		}
		public String getFilePath() {
			return filePath;
		}
		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

}
