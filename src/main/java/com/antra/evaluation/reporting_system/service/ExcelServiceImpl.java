package com.antra.evaluation.reporting_system.service;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.evaluation.reporting_system.pojo.api.ExcelResponse;
import com.antra.evaluation.reporting_system.pojo.report.ExcelData;
import com.antra.evaluation.reporting_system.pojo.report.ExcelFile;
import com.antra.evaluation.reporting_system.repo.ExcelConstant;
import com.antra.evaluation.reporting_system.repo.ExcelRepository;

@Service
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    ExcelRepository excelRepository;
    
    @Autowired
    ExcelGenerationService excelGenService;

    @Override
    public InputStream getExcelBodyById(String id) {

        Optional<ExcelFile> fileInfo = excelRepository.getFileById(id);
        File file = new File(ExcelConstant.DIR+fileInfo.get().getFileName());
        InputStream is =  null;
        if(file.exists()) {
        	try {
				is = new DataInputStream(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
        }
      
        return is;
    }

	@Override
	public ExcelResponse createExcel(ExcelData data) {
		ExcelResponse response = new ExcelResponse();
		ExcelFile file = new ExcelFile();
		try {
			response = excelGenService.generateExcelReport(data);
			file.setFileId(response.getFileId());
			file.setFileName(response.getFileName());
			file.setFileSize(response.getFileSize());
			
			excelRepository.saveFile(file);
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		return response;
	}

	@Override
	public ExcelResponse deleteExcel(String id) {
		ExcelFile exl = getExcelFileById( id);
		ExcelResponse response = new ExcelResponse();
		
		File file = new File(exl.getFilePath());
        
        if(file.delete()) 
        { 
        	response.setFileName(exl.getFileName());
        	
        } 
        else
        { 
            System.out.println("Failed to delete the file"); 
        } 
		
		excelRepository.deleteFile(id);
		return response;
	}

	@Override
	public ExcelFile getExcelFileById(String id) {
		ExcelFile file =  excelRepository.getFileById(id).get();
		file.setFilePath(ExcelConstant.DIR+file.getFileName());
		return file;
	}

	@Override
	public List<ExcelResponse> listExels() {
		ExcelResponse res = null;
		List<ExcelResponse> list = new ArrayList<>();
		for(ExcelFile f :excelRepository.getFiles()) {
			res = new ExcelResponse();
			res.setFileId(f.getFileId());
			res.setFileName(f.getFileName());
			res.setFileSize(f.getFileSize());
			res.setGeneratedTime(f.getGeneratedTime());
			list.add(res);
		}
		return list;
	}

}
