package com.antra.evaluation.reporting_system.repo;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.antra.evaluation.reporting_system.pojo.report.ExcelFile;

@Repository
public class ExcelRepositoryImpl implements ExcelRepository {
	
	

    Map<String, ExcelFile> excelData = new ConcurrentHashMap<>();
    
    public ExcelRepositoryImpl() throws IOException {
    	//ExcelFile exlFile = null;
    	String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	Files.walkFileTree(Paths.get(ExcelConstant.DIR), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
              throws IOException {
                if (!Files.isDirectory(file)) {
                	ExcelFile exlFile = new ExcelFile();
                	FileTime fileTime = attrs.creationTime();
        			

        			Calendar cal = Calendar.getInstance();
        			cal.setTimeInMillis(fileTime.toMillis());
                	exlFile.setFileName(file.getFileName().toString());
                	exlFile.setFileId(getFileName(file.getFileName().toString()));
                	exlFile.setFileSize(file.toFile().length()+ "  bytes");
                	exlFile.setGeneratedTime(simpleDateFormat.format(cal.getTime()));
                	excelData.put(exlFile.getFileId(), exlFile);
                    
                }
                return FileVisitResult.CONTINUE;
            }
        });
    	
    	
    }

    @Override
    public Optional<ExcelFile> getFileById(String id) {
        return Optional.ofNullable(excelData.get(id));
    }

    @Override
    public ExcelFile saveFile(ExcelFile file) {
    	excelData.put(file.getFileId(), file);
        return file;
    }

    @Override
    public ExcelFile deleteFile(String id) {
    	ExcelFile exlFile = getFileById(id).get();
    	excelData.remove(id);
        return exlFile;
    }

    @Override
    public List<ExcelFile> getFiles() {
        return excelData.values().stream().collect(Collectors.toList());
    }
    
    private String getFileName(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf('.'));
	}
}

